#!/bin/bash
set -evx

mkdir ~/.ioncoin

# safety check
if [ ! -f ~/.ioncoin/.ioncoin.conf ]; then
  cp share/ioncoin.conf.example ~/.ioncoin/ioncoin.conf
fi
